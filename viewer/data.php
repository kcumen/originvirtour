<?php
if((isset($_GET['t'])) && (isset($_GET['r']))) {
    $tour = $_GET['t'];
    $room = $_GET['r'];
} else {
}
?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Estadísticas Virtour</title>
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/sb-admin-2.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/fontawesome-free/css/all.min.css">
    <link rel='stylesheet' type="text/css" id="font_backend_link" href="https://fonts.googleapis.com/css?family=Roboto">
     
    <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="vendor/chart.js/Chart.min.js"></script>
</head>
    <body id="page-top"  style="background-color:transparent;">
        <style id="style_css">
            *{ font-family: 'Roboto', sans-serif; }
            .card{ opacity: .8;}
            #stand_name{
                color:#44148d;
            }
            #legend-container ul{
                list-style-type: none;
                font-size: 12px;
                white-space: nowrap;
            }
            #legend-container li span{
                width:12px;
                height: 12px;
                display: inline-block;
                margin: 0 5px 8px 0;
                vertical-align: -9.4px;
            }
            #totalAccess{
                position: absolute;
                top:50%;
                left:50%;
                transform: translate(-50%, -50%);
            }
        </style>

        <div id="wrapper">
            <div id="content-wrapper" class="d-flex flex-column" style="background-color:transparent;">
                <div id="content">
                    <div class="container-fluid">


                            <div class="card shadow p-2 mb-3">
                                <h1 id="stand_name" class="h3 mb-0 text-center"></h1>
                            </div>
                            <div class="row">
                               <div class="col-12">
                                   <div class="row justify-content-center">
                                       <div class="col-6 ">
                                           <div class="card shadow mb-4">
                                               <div class="card-body" style="min-height: 11.5rem;">
                                                   <h6 class="m-0 font-weight-bold text-primary pb-3 text-center"><?php echo _("Visitas Recibidas"); ?></h6>
                                                   <div id="chart_rooms_access" class="text-center display-3" style="color:#71c413;"></div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="col-6 ">
                                           <div class="card shadow mb-4" style="min-height: 11.5rem;">
                                               <div class="card-body">
                                                   <h6 class="m-0 font-weight-bold text-primary pb-3 text-center"><?php echo _("Tiempo de permanencia (segundos)"); ?></h6>
                                                   <div id="chart_rooms_time" class="text-center display-3" style="color:#5292c2;"></div>
                                               </div>
                                           </div>
                                       </div>

                                   </div>
                               </div>


                                <div class="col-md-12 ">
                                    <div class="card shadow mb-4">
                                        <h6 class="m-0 font-weight-bold text-primary pb-3 text-center"><?php echo _("Interacciones realizadas"); ?></h6>
                                        <div class="card-body">
                                            <div class="row justify-content-center align-items-stretch">
                                                <div class="col-6">
                                                    <canvas id="chart_poi_views"></canvas>
                                                    <div id="totalAccess" class="text-primary font-weight-bolder display-4"></div>
                                                </div>
                                                <div class="col">
                                                    <div id="legend-container"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    <div class="card shadow mb-4">
                                        <div class="card-body">
                                            <h6 class="m-0 font-weight-bold text-primary pb-3 text-center"><?php echo _("Interacciones redes sociales"); ?></h6>
                                            <div id="socialNet" class="row justify-content-center"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                    </div>
                </div>
            </div>
        </div>
        <script>
            (function($) {
                "use strict"; // Start of use strict
                Chart.defaults.global.defaultFontFamily = 'Roboto', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Nunito,"Helvetica Neue",Arial,sans-serif';
                var id_virtualtour = <?php echo $tour; ?>;
                var vtroom = <?php echo $room; ?>;
                console.log('Tour id: '+id_virtualtour, 'Room id: '+vtroom);
                $(document).ready(function () {
                    get_statistics('chart_rooms_access',id_virtualtour,vtroom);
                    get_statistics('chart_rooms_time',id_virtualtour,vtroom);
                    get_statistics('chart_poi_views',id_virtualtour,vtroom);
                });
                window.get_statistics = function (elem,id_virtualtour,vtroom) {
                    $.ajax({
                        url: "ajax/get_statistics.php",
                        type: "POST",
                        data: {
                            id_virtualtour: id_virtualtour,
                            elem: elem,
                            vtroom: vtroom
                        },
                        async: true,
                        success: function (json) {
                            var rsp = JSON.parse(json);
                            switch (elem) {
                                case 'chart_rooms_access':
                                    var stname = rsp.stand;
                                    $('#stand_name').html(stname.slice(stname.lastIndexOf('|')+1));
                                    $('#chart_rooms_access').html(rsp.total);
                                    break;
                                case 'chart_rooms_time':
                                    $('#chart_rooms_time').html(rsp.total);
                                    break;
                                case 'chart_poi_views':
                                    console.log(rsp);
                                    var total_access = rsp.total_poi;
                                    var new_labels = Array({'galeria_imagenes':'Galería de imagenes','whatsapp_principal':'WhatsApp 1','whatsapp_secundario':'WhatsApp 2','imagen_pantalla_vertical_izquierda':'Pantalla vertical izq.','imagen_pantalla_vertical_derecha':'Pantalla vertical der.','imagen_pared_d1':'Cuadro #1','imagen_pared_d2':'Cuadro #2','portafolio':'Portafolio','url_video_principal':'Video','url_agendamiento':'Agenda','url_pantalla_tactil':'Pantalla táctil',});
                                    var social = Array('facebook','instagram','youtube','linkedin','twitter');
                                    var c_social = Array('#1877F2','#E4405F','#CD201F','#1877F2','#0A66C2');
                                    var info = Array('(+','pdf','jpg','www','png');

                                    var color = Array('#d4c431','#72aac0','#7f72c1','#5dd1de','#dd735d','#60a1ff','#ff3234','#88C076','#434954','#21A1A4','#FB6412');
                                    var data = {};
                                    var labels = Array();
                                    var datasets = Array();
                                    var datacount = Array();
                                    var html_social = '';
                                    var totalPois = rsp.pois.length;
                                    var validPois=0;
                                    (async () => {
                                        console.log('totalPois: '+ totalPois);
                                        const dts = await jQuery.each(rsp.pois,async function(index, poi) {
                                            
                                            var content = poi.content;
                                            if((content != '') && (content != null)){
                                                validPois++;
                                            
                                                var type = poi.type;
                                                var api_prop = poi.api_property;
                                                if(type === "gallery") content = poi.type;
                                                content = $($.parseHTML(content)).text();
                                                const has_social = await social.some(function(v) { return api_prop.indexOf(v) >= 0;} );
                                                if(has_social){
                                                    var icon;
                                                    var style;
                                                    $.each(social, function(i,v){
                                                        if(content.indexOf(v) >=0){
                                                            (v !== 'linkedin') ? icon=v+'-square' : icon=v;
                                                            style = c_social[i];
                                                        }
                                                    });
                                                    var total = poi.access_count;
                                                    total_access -= total;
                                                    html_social +='<div class="col-auto social mb-2" style="color:'+style+' !important; ">\n' +
                                                                '   <i class="fab fa-'+icon+' mr-3 mb-0 h3" style="vertical-align:middle;"></i><strong style="font-size:130%; font-weight:bolder; vertical-align:middle;">'+total+'</strong>\n' +
                                                                '</div>';
                                                }else{
                                                    console.log(new_labels[0][api_prop] + '= '+poi.access_count);
                                                    labels.push(new_labels[0][api_prop]);
                                                    datacount.push(poi.access_count);
                                                }
                                            }else{
                                                totalPois--;
                                                total_access -= poi.access_count;
                                            }                                       
                                            if(validPois === totalPois){
                                                data['labels'] = labels;
                                                datasets.push({'data': datacount, "backgroundColor":color, "borderColor":color, 'borderWidth':1, 'cutout':'90%'});
                                                data['datasets'] = datasets;
                                                $('#socialNet').html(html_social);  
                                                $('#totalAccess').html(total_access);                                           
                                            }
                                        });//end each

                                        var chart = document.getElementById("chart_poi_views");
                                        var ctx = new Chart(chart, {
                                            type: 'doughnut',
                                            data: data,
                                            options: {
                                                responsive: true,
                                                aspectRatio: 1,
                                                maintainAspectRatio: true,
                                                legend: false,
                                                legendCallback: function(chart) {
                                                    var ul =document.createElement('ul');ul.classList.add('p-0');
                                                    var borderColor = chart.data.datasets[0].borderColor;
                                                    var dataValue = chart.data.datasets[0].data;
                                                    chart.data.labels.forEach(function(label,index){
                                                        ul.innerHTML += `<li class="text-left"><span style="background-color: ${borderColor[index]}";></span> <strong>${label}: </strong> <strong class=" text-primary">${dataValue[index]}</strong></li>`;
                                                    });
                                                    return ul.outerHTML;
                                                }
                                            }
                                        });
                                        var legend = document.getElementById('legend-container');                                        
                                        legend.innerHTML = ctx.generateLegend();
                                    })();
                                    break;
                            }
                        }
                    });
                }
                
                
            })(jQuery); // End of use strict
        </script>
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
    
    </body>
</html>