<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();
session_start();
// require_once("../functions.php");
require_once("../../db/connection.php");
$id_virtualtour = $_POST['id_virtualtour'];
$vtroom = $_POST['vtroom'];
$elem = $_POST['elem'];


$stats = array();

switch ($elem) {
    case 'chart_rooms_access':
        $stats['labels'] = array();
        $stats['data'] = array();
        $query = "SELECT r.name,r.access_count as num 
                    FROM svt_rooms as r
                    WHERE r.id_virtualtour=$id_virtualtour
                    AND r.id = $vtroom 
                    GROUP BY r.id
                    ORDER BY r.priority;";
        $result = $mysqli->query($query);
        if($result) {
            if($result->num_rows>0) {
                while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                    $stats = array('stand' => strtoupper($row['name']), 'total' => $row['num']);                   
                }
            }
        }
        break;
    case 'chart_rooms_time':
        $stats['labels'] = array();
        $stats['data'] = array();
        $query = "SELECT r.name,AVG(time) as num 
                    FROM svt_rooms_access_log as l
                    JOIN svt_rooms as r on r.id=l.id_room
                    WHERE r.id_virtualtour=$id_virtualtour 
                    AND r.id = $vtroom
                    GROUP BY l.id_room
                    ORDER BY r.priority;";
        $result = $mysqli->query($query);
        if($result) {
            if($result->num_rows>0) {
                while($row = $result->fetch_array(MYSQLI_ASSOC)) {                    
                    $stats = array('total' => round($row['num']));
                }
            }
        }
        break;
    case 'chart_poi_views':
        $stats['pois'] = array();
        $stats['total_poi'] = 0;
        $query = "SELECT p.type,p.api_property,p.content,p.access_count
                    FROM svt_pois as p
                    JOIN svt_rooms as r ON r.id=p.id_room
                    WHERE r.id_virtualtour=$id_virtualtour 
                    AND r.id = $vtroom
                    AND p.access_count>0 
                    GROUP BY p.id
                    ORDER BY p.access_count DESC;";
        $result = $mysqli->query($query);
        if($result) {
            if($result->num_rows>0) {
                while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                    $stats['total_poi'] = $stats['total_poi'] + $row['access_count'];
                    if(empty($row['type'])) $row['type']='';
                    array_push($stats['pois'],$row);
                }
            }
        }
        break;
}
ob_end_clean();
echo json_encode($stats);