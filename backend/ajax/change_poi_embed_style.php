<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();
session_start();
if((($_SERVER['SERVER_ADDR']=='5.9.29.89') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) || ($_SESSION['svt_si']!=session_id())) {
    die();
}
require_once("../../db/connection.php");
$id = $_POST['id'];
$yaw = $_POST['yaw'];
$pitch = $_POST['pitch'];
$embed_type = $_POST['embed_type'];

// VTR
$is_template = $_POST['is_template'];
$poi_arr = array();
$poi = $mysqli->query("SELECT * FROM svt_pois WHERE id = $id");
foreach($poi as $key => $val){
    $content = str_replace("'","\'",$val);
    $poi_arr[$key] = $content;
}
//  VTR

if(empty($embed_type)) {
    $query = "UPDATE svt_pois SET embed_type=NULL WHERE id=$id;";
} else {
    $coord_1 = ($pitch+5).",".($yaw-10);
    $coord_2 = ($pitch-5).",".($yaw-10);
    $coord_3 = ($pitch+5).",".($yaw+10);
    $coord_4 = ($pitch-5).",".($yaw+10);
    $embed_coords = "$coord_1|$coord_2|$coord_3|$coord_4";
    $embed_size = "300,150";
    $content_q_add = "";
    switch($_POST['embed_type']) {
        case 'gallery':
        case 'video':
        case 'video_transparent':
        case 'link':
            $embed_content="";
            $content_q_add = ",type=NULL,content=NULL";
            break;
        case 'selection':
            $embed_content="border-width:3px;";
            break;
        default:
            $embed_content="";
            break;
    }
    $query = "UPDATE svt_pois SET embed_type='$embed_type',embed_size='$embed_size',embed_coords='$embed_coords',embed_content='$embed_content' $content_q_add WHERE id=$id;";
}
$result=$mysqli->query($query);
if($result) {
    // VTR
    if($is_template === '1'){
        $rooms = $mysqli->query("SELECT * FROM `svt_rooms` WHERE `id_wp_post` > 1");
        $old_pitch = $pitch;
        $old_yaw = $yaw;
        
        if($rooms->num_rows > 0) {
            while($row = $rooms->fetch_array(MYSQLI_ASSOC)) {
                $room_id = $row['id'];
                $mysqli->query("UPDATE svt_pois SET content=NULL,embed_content='$embed_content',embed_type='$embed_type',embed_size='$embed_size',embed_coords='$embed_coords' WHERE id_room=$room_id AND CAST(pitch AS DECIMAL)=CAST($old_pitch AS DECIMAL) AND CAST(yaw AS DECIMAL)=CAST($old_yaw AS DECIMAL);");
            }
        }
    }
    //  VTR
    ob_end_clean();
    echo json_encode(array("status"=>"ok"));
} else {
    ob_end_clean();
    echo json_encode(array("status"=>"error"));
}
