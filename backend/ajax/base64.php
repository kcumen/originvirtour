<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();

if(($_SERVER['SERVER_ADDR']=='162.214.227.82') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
ini_set('max_execution_time', 9999);


$obj = trim(file_get_contents("php://input"));
$decoded = json_decode($obj, true);
$url = $decoded['url'];

$img_64 = base64_encode(file_get_contents($url));

echo json_encode(array("status"=>"ok", 'data64'=> $img_64));
