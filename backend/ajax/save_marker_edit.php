<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();
session_start();
if((($_SERVER['SERVER_ADDR']=='5.9.29.89') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) || ($_SESSION['svt_si']!=session_id())) {
    die();
}
require_once("../../db/connection.php");
$id_marker = $_POST['id'];
$id_room_target = $_POST['id_room_target'];
$yaw = $_POST['yaw'];
$pitch = $_POST['pitch'];
if($yaw=='') $yaw = 'NULL';
if($pitch=='') $pitch = 'NULL';
$lookat = $_POST['lookat'];

// VTR
$is_template = $_POST['is_template'];
$id_pabellon = $_POST['id_pabellon'];
$room_type='';
$room_type_query = $mysqli->query("SELECT `room_type` FROM `svt_rooms` WHERE `id` = '$id_pabellon'");
while ($row = $room_type_query->fetch_array(MYSQLI_ASSOC)) 
{
    $room_type = $row['room_type'];  
}
$marker_arr = array();
$marker = $mysqli->query("SELECT * FROM svt_markers WHERE id = '$id_marker'");
foreach($marker as $key => $val){
    $value = str_replace("'","\'",$val);
    $marker_arr[$key] = $value;
}
// VTR

$query = "UPDATE svt_markers SET id_room_target=$id_room_target,yaw_room_target=$yaw,pitch_room_target=$pitch,lookat=$lookat WHERE id=$id_marker;";
$result = $mysqli->query($query);

if($result) {
    // VTR
    $rooms = '';
    if($room_type === 'pabellon'){
        $rooms = $mysqli->query("SELECT * FROM `svt_rooms` WHERE `room_type` = 'pabellon' AND `id` != '$id_pabellon'");
    }else{
        if($is_template === '1'){
            $rooms = $mysqli->query("SELECT * FROM `svt_rooms` WHERE `id_wp_post` > 1");
        }
    }
    $old_pitch = $marker_arr[0]['pitch'];
    $old_yaw = $marker_arr[0]['yaw'];
    if($rooms->num_rows > 0) {
        while($row = $rooms->fetch_array(MYSQLI_ASSOC)) {
            $room_id = $row['id'];
           $mysqli->query("UPDATE svt_markers SET yaw_room_target=$yaw,pitch_room_target=$pitch,lookat=$lookat WHERE id_room=$room_id AND CAST(pitch AS DECIMAL)=CAST($old_pitch AS DECIMAL) AND CAST(yaw AS DECIMAL)=CAST($old_yaw AS DECIMAL);");
       }
   }
   // VTR
    ob_end_clean();
    echo json_encode(array("status"=>"ok"));
} else {
    ob_end_clean();
    echo json_encode(array("status"=>"error"));
}

