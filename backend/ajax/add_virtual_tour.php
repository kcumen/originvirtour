<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();
session_start();
if((($_SERVER['SERVER_ADDR']=='5.9.29.89') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) || ($_SESSION['svt_si']!=session_id())) {
    die();
}
require_once("../../db/connection.php");
$id_user = $_POST['id_user'];
$name = str_replace("'","\'",strip_tags($_POST['name']));
$author = str_replace("'","\'",strip_tags($_POST['author']));
$external = $_POST['external'];

$query = "INSERT INTO svt_virtualtours(id_user,date_created,name,author,hfov,min_hfov,max_hfov,external)
            VALUES($id_user,NOW(),'$name','$author',100,50,100,$external); ";
$result = $mysqli->query($query);

if($result) {
    $insert_id = $mysqli->insert_id;
    $_SESSION['id_virtualtour_sel'] = $insert_id;
    $_SESSION['name_virtualtour_sel'] = $_POST['name'];
    $code = md5($insert_id);
    $mysqli->query("UPDATE svt_virtualtours SET code='$code' WHERE id=$insert_id;");
    $query = "SELECT id FROM svt_advertisements WHERE auto_assign=1 LIMIT 1;";
    $result = $mysqli->query($query);
    if($result) {
        if($result->num_rows==1) {
            $row=$result->fetch_array(MYSQLI_ASSOC);
            $id_ads=$row['id'];
            $mysqli->query("INSERT INTO svt_assign_advertisements(id_advertisement,id_virtualtour) VALUES($id_ads,$insert_id);");
        }
    }
    ob_end_clean();
    echo json_encode(array("status"=>"ok","id"=>$insert_id));
} else {
    ob_end_clean();
    echo json_encode(array("status"=>"error"));
}

