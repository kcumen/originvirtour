<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();
session_start();
if((($_SERVER['SERVER_ADDR']=='5.9.29.89') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) || ($_SESSION['svt_si']!=session_id())) {
    die();
}
require_once("../../db/connection.php");
require_once("../functions.php");
$id_marker = $_POST['id_marker'];

if(!check_can_delete($_SESSION['id_user'],$_SESSION['id_virtualtour_sel'])) {
    ob_end_clean();
    echo json_encode(array("status"=>"error"));
    die();
}

// VTR
$is_template = $_POST['is_template'];
$id_pabellon = $_POST['id_pabellon'];
$room_type='';
$room_type_query = $mysqli->query("SELECT `room_type` FROM `svt_rooms` WHERE `id` = '$id_pabellon'");
while ($row = $room_type_query->fetch_array(MYSQLI_ASSOC)) 
{
    $room_type = $row['room_type'];  
}
$marker_arr = array();
$marker = $mysqli->query("SELECT * FROM svt_markers WHERE id = '$id_marker'");
foreach($marker as $key => $val){
    $value = str_replace("'","\'",$val);
    $marker_arr[$key] = $value;
}
// VTR

$query = "DELETE FROM svt_markers WHERE id=$id_marker; ";
$result = $mysqli->query($query);

if($result) {
    // VTR
    $rooms = '';
    if($room_type === 'pabellon'){
        $rooms = $mysqli->query("SELECT * FROM `svt_rooms` WHERE `room_type` = 'pabellon' AND `id` != '$id_pabellon'");
    }else{
        if($is_template === '1'){
            $rooms = $mysqli->query("SELECT * FROM `svt_rooms` WHERE `id_wp_post` > 1");
        }
    }
    $old_pitch = $marker_arr[0]['pitch'];
    $old_yaw = $marker_arr[0]['yaw'];
    if($rooms->num_rows > 0) {
        while($row = $rooms->fetch_array(MYSQLI_ASSOC)) {
            $room_id = $row['id'];
            $mysqli->query("DELETE FROM svt_markers WHERE id_room=$room_id AND CAST(pitch AS DECIMAL)=CAST($old_pitch AS DECIMAL) AND CAST(yaw AS DECIMAL)=CAST($old_yaw AS DECIMAL);");
        }
    }
    // VTR
    $mysqli->query("ALTER TABLE svt_markers AUTO_INCREMENT = 1;");
    ob_end_clean();
    echo json_encode(array("status"=>"ok"));
} else {
    ob_end_clean();
    echo json_encode(array("status"=>"error"));
}
