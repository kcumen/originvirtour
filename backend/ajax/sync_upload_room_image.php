<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();
session_start();
if(($_SERVER['SERVER_ADDR']=='162.214.227.82') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
ini_set("memory_limit",-1);
ini_set('max_execution_time', 9999);
ini_set('max_input_time', 9999);
require_once(dirname(__FILE__).'/ImageResizeException.php');
require_once(dirname(__FILE__).'/ImageResize.php');
use \Gumlet\ImageResize;
require_once(dirname(__FILE__).'/../functions.php');
$settings = get_settings();
$user_info = get_user_info($_SESSION['id_user']);
if(!empty($user_info['language'])) {
    set_language($user_info['language'],$settings['language_domain']);
} else {
    set_language($settings['language'],$settings['language_domain']);
}

// $compress_jpg = $_SESSION['compress_jpg'];
// $max_width_compress = $_SESSION['max_width_compress'];
// if($compress_jpg=="") $compress_jpg=90;
// if($max_width_compress=="") $max_width_compress=0;
// $maxFileSize = 2 * 1024 * 1024;

// ---------------------------------------------------------------
$destinationFolder = $_SERVER['DOCUMENT_ROOT'] .'/backend/tmp_panoramas/';
$postdata = file_get_contents("php://input");
if(empty($postdata))exit;

$request = json_decode($postdata);
$id_virtualtour = $request->id_virtualtour;
$total_pano = $request->total_pano;
$panocount = $request->panocount;
$roomID = $request->id;
$url = $request->url;
$file_name = substr($url, strrpos($url,'/') + 1);//nombre con extension
$ext = explode('.',$file_name);
$ext = end($ext);

$tmpname = "pano_" . time() . '.' . $ext;
$destinationPath = "$destinationFolder$tmpname";
if(strpos($url,'scaled') >=0){
    $url = substr($url,0,strrpos($url,'-'));
    $url = $url. '.' . $ext;
}
$success = file_put_contents($destinationPath, file_get_contents($url));
$panorama_image = "tmp_panoramas/". $tmpname;


$name_image = str_replace("tmp_panoramas/","",$panorama_image);
$path_source = dirname(__FILE__).'/../tmp_panoramas/'.$name_image;
$path_dest = dirname(__FILE__).'/../../viewer/panoramas/'.$name_image;
if (!file_exists(dirname(__FILE__).'/../../viewer/panoramas/')) {
    mkdir(dirname(__FILE__).'/../../viewer/panoramas/', 0775);
}
if (!file_exists(dirname(__FILE__).'/../../viewer/panoramas/thumb/')) {
    mkdir(dirname(__FILE__).'/../../viewer/panoramas/thumb/', 0775);
}
if (!file_exists(dirname(__FILE__).'/../../viewer/panoramas/mobile/')) {
    mkdir(dirname(__FILE__).'/../../viewer/panoramas/mobile/', 0775);
}
if (!file_exists(dirname(__FILE__).'/../../viewer/panoramas/preview/')) {
    mkdir(dirname(__FILE__).'/../../viewer/panoramas/preview/', 0775);
}
if(copy($path_source,$path_dest)) {
    unlink($path_source);
    include("../../services/generate_thumb.php");
    include("../../services/generate_pano_mobile.php");

    $query ="UPDATE svt_rooms SET panorama_image='$name_image' WHERE id = $roomID;";
    $result = $mysqli->query($query);
    
    if($result){
        if($panocount === $total_pano - 1)generate_multires(false,$id_virtualtour);
        ob_end_clean();
        echo json_encode(array("upload" => 'ok', "thumb"=>$name_image, "panocount"=>'$panocount', "total_pano"=>'$total_pano' ));
    }
} else {
    ob_end_clean();
    echo json_encode(array("status"=>"error image"));
    die();
}
 