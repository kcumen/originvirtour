<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();
session_start();
if((($_SERVER['SERVER_ADDR']=='5.9.29.89') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) || ($_SESSION['svt_si']!=session_id())) {
    die();
}
require_once("../../db/connection.php");
$id = $_POST['id'];
$content_type = $_POST['content_type'];
// VTR
$is_template = $_POST['is_template'];
$poi_arr = array();
$poi = $mysqli->query("SELECT * FROM svt_pois WHERE id = '$id'");
foreach($poi as $key => $val){
    $value = str_replace("'","\'",$val);
    $poi_arr[$key] = $value;
}
// VTR
if(empty($content_type)) {
    $query = "UPDATE svt_pois SET type=NULL,content=NULL WHERE id=$id;";
} else {
    $query = "UPDATE svt_pois SET type='$content_type',content=NULL WHERE id=$id;";
}
$result=$mysqli->query($query);
if($result) {
    // VTR
    if($is_template === '1'){        
        $rooms = $mysqli->query("SELECT * FROM `svt_rooms` WHERE `id_wp_post` > 1");
        $old_pitch = $poi_arr[0]['pitch'];
        $old_yaw = $poi_arr[0]['yaw'];
        if($rooms->num_rows > 0) {
            while($row = $rooms->fetch_array(MYSQLI_ASSOC)) {
                $room_id = $row['id'];
                $mysqli->query("UPDATE svt_pois SET type='$content_type',embed_type='$embed_type' WHERE id_room=$room_id AND CAST(pitch AS DECIMAL)=CAST($old_pitch AS DECIMAL) AND CAST(yaw AS DECIMAL)=CAST($old_yaw AS DECIMAL);");
            }
        }
    } 
    // VTR
    ob_end_clean();
    echo json_encode(array("status"=>"ok"));
} else {
    ob_end_clean();
    echo json_encode(array("status"=>"error"));
}
