<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();
session_start();
if((($_SERVER['SERVER_ADDR']=='5.9.29.89') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) || ($_SESSION['svt_si']!=session_id())) {
    die();
}
require_once("../../db/connection.php");
$id_poi = $_POST['id'];
$yaw = $_POST['yaw'];
$pitch = $_POST['pitch'];
$size_scale = $_POST['size_scale'];
$rotateX = $_POST['rotateX'];
$rotateZ = $_POST['rotateZ'];
$embed_coords = $_POST['embed_coords'];
$embed_size = $_POST['embed_size'];
if(empty($embed_coords)) $embed_coords = "NULL"; else $embed_coords="'$embed_coords'";
if(empty($embed_size)) $embed_size = "NULL"; else $embed_size="'$embed_size'";
$transform3d = $_POST['transform3d'];
if(!isset($transform3d)) $transform3d=1;

// VTR
$is_template = $_POST['is_template'];
$cloned = $_POST['cloned'];
$poi_arr = array();
$poi = $mysqli->query("SELECT * FROM svt_pois WHERE id = '$id_poi'");
foreach($poi as $key => $val){
    $content = str_replace("'","\'",$val);
    $poi_arr[$key] = $content;
}
// VTR

$query = "UPDATE svt_pois SET yaw=$yaw,pitch=$pitch,size_scale=$size_scale,rotateX=$rotateX,rotateZ=$rotateZ,embed_coords=$embed_coords,embed_size=$embed_size,transform3d=$transform3d WHERE id=$id_poi;";
$result = $mysqli->query($query);

if($result) {
    // VTR
    if($is_template === '1' ){
        $rooms = $mysqli->query("SELECT * FROM `svt_rooms` WHERE `id_wp_post` > 1");
        $old_pitch = $poi_arr[0]['pitch'];
        $old_yaw = $poi_arr[0]['yaw'];
        $id_template = $poi_arr[0]['id_room'];
        if($rooms->num_rows > 0) {
            while($row = $rooms->fetch_array(MYSQLI_ASSOC)) {
                $room_id = $row['id'];
                if($room_id != $id_template){
                    $mysqli->query("UPDATE svt_pois SET yaw=$yaw,pitch=$pitch,size_scale=$size_scale,rotateX=$rotateX,rotateZ=$rotateZ,embed_coords=$embed_coords,embed_size=$embed_size WHERE id_room=$room_id AND CAST(pitch AS DECIMAL)=CAST($old_pitch AS DECIMAL) AND CAST(yaw AS DECIMAL)=CAST($old_yaw AS DECIMAL);");
                }
            }
        }
    }
    // VTR
    ob_end_clean();
    echo json_encode(array("status"=>"ok","q"=>$query,"is_template"=>$is_template,"cloned"=>$cloned));
} else {
    ob_end_clean();
    echo json_encode(array("status"=>"error"));
}

