<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();
session_start();
if(($_SERVER['SERVER_ADDR']=='162.214.227.82') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
ini_set('max_execution_time', 9999);
require_once("../../db/connection.php");
require_once("../functions.php");

$settings = get_settings();
$user_info = get_user_info($_SESSION['id_user']);
if(!empty($user_info['language'])) {
    set_language($user_info['language'],$settings['language_domain']);
} else {
    set_language($settings['language'],$settings['language_domain']);
}

$obj = trim(file_get_contents("php://input"));
$decoded = json_decode($obj, true);
$id_room = $decoded['id'];
$roomname = $decoded['roomname'];
$post_id = $decoded['post_id'];

$mysqli->query("CREATE TEMPORARY TABLE svt_room_tmp SELECT * FROM svt_rooms WHERE id = $id_room;");
$mysqli->query("UPDATE svt_room_tmp SET id=(SELECT MAX(id)+1 as id FROM svt_rooms), priority=(SELECT MAX(priority)+1 as 'priority' FROM svt_rooms),name='$roomname',thumb_image='',is_template='0',id_wp_post='$post_id',id_map=NULL,map_top=NULL,map_left=NULL,lat=NULL,lon=NULL,access_count=0,multires_status=0;");
$mysqli->query("INSERT INTO svt_rooms SELECT * FROM svt_room_tmp;");
$id_room_new = $mysqli->insert_id;
$mysqli->query("DROP TEMPORARY TABLE IF EXISTS svt_room_tmp;");

$vt_room_id;
$get_id = $mysqli->query("SELECT MAX(id) as id FROM svt_rooms");
if($get_id) {
    if ($get_id->num_rows > 0) {
        while ($row = $get_id->fetch_array(MYSQLI_ASSOC)) {
            $vt_room_id = $row['id'];
        }
    }
}

$result = $mysqli->query("SELECT id FROM svt_rooms_alt WHERE id_room=$id_room;");
if($result) {
    if($result->num_rows>0) {
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $id_room_alt = $row['id'];
            $mysqli->query("CREATE TEMPORARY TABLE svt_rooms_alt_tmp SELECT * FROM svt_rooms_alt WHERE id = $id_room_alt;");
            $mysqli->query("UPDATE svt_rooms_alt_tmp SET id=(SELECT MAX(id)+1 as id FROM svt_rooms_alt),id_room=$id_room_new;");
            $mysqli->query("INSERT INTO svt_rooms_alt SELECT * FROM svt_rooms_alt_tmp;");
            $mysqli->query("DROP TEMPORARY TABLE IF EXISTS svt_rooms_alt_tmp;");
        }
    }
}

$array_pois = array();
$result = $mysqli->query("SELECT id FROM svt_pois WHERE id_room=$id_room;");
if($result) {
    if($result->num_rows>0) {
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $id_poi = $row['id'];
            $mysqli->query("CREATE TEMPORARY TABLE svt_poi_tmp SELECT * FROM svt_pois WHERE id = $id_poi;");
            $mysqli->query("UPDATE svt_poi_tmp SET id=(SELECT MAX(id)+1 as id FROM svt_pois),id_room=$id_room_new;");
            $mysqli->query("INSERT INTO svt_pois SELECT * FROM svt_poi_tmp;");
            $id_poi_new = $mysqli->insert_id;
            $array_pois[$id_poi] = $id_poi_new;
            $mysqli->query("DROP TEMPORARY TABLE IF EXISTS svt_poi_tmp;");
        }
    }
}

foreach ($array_pois as $id_poi=>$id_poi_new) {
    $result = $mysqli->query("SELECT id FROM svt_poi_gallery WHERE id_poi=$id_poi;");
    if($result) {
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $id_poi_gallery = $row['id'];
                $mysqli->query("CREATE TEMPORARY TABLE svt_poi_gallery_tmp SELECT * FROM svt_poi_gallery WHERE id = $id_poi_gallery;");
                $mysqli->query("UPDATE svt_poi_gallery_tmp SET id=(SELECT MAX(id)+1 as id FROM svt_poi_gallery),id_poi=$id_poi_new;");
                $mysqli->query("INSERT INTO svt_poi_gallery SELECT * FROM svt_poi_gallery_tmp;");
                $mysqli->query("DROP TEMPORARY TABLE IF EXISTS svt_poi_gallery_tmp;");
            }
        }
    }
}

ob_end_clean();
echo json_encode(array("status"=>"ok", 'vt_room_id'=> $vt_room_id, 'Stand'=> $roomname));

