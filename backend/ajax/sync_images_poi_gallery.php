<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();
session_start();
if(($_SERVER['SERVER_ADDR']=='162.214.227.82') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$id_poi = $_POST['id'];
$gallery = $_POST['gallery'];

$getimgs = "SELECT * FROM svt_poi_gallery WHERE id_poi=$id_poi;";
$imgresult = $mysqli->query($getimgs);

if($imgresult->num_rows > 0) {
    $query = "DELETE FROM svt_poi_gallery WHERE id_poi=$id_poi;";
    $result = $mysqli->query($query);
    $mysqli->query("ALTER TABLE svt_poi_gallery AUTO_INCREMENT = 1;");
    require_once("../../services/clean_images.php");
    ob_end_clean();
}


$syncresult;
$priority = 0;
if(count($gallery) > 0){
    foreach($gallery as $key => $val){
        $image = str_replace("'","\'",$val);
        $image = $val;
        $sync = "INSERT INTO svt_poi_gallery(id_poi,image,priority) VALUES($id_poi,'$image',$priority); ";
        $syncresult = $mysqli->query($sync);
        $priority++;
    }
}else{
    exit;
}

if($syncresult) {    
    echo json_encode(array("status"=>"ok"));
} else {   
    echo json_encode(array("status"=>"error", 'id_poi'=>$id_poi));
}