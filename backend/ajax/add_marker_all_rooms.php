<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ob_start();
session_start();
if(($_SERVER['SERVER_ADDR']=='5.9.29.89') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer']) && ($_SESSION['id_user']==1)) {
    //DEMO CHECK
    die();
}
require_once("../../db/connection.php");
$marker_id = $_POST['marker_ID'];

$result = $mysqli->query("SELECT * FROM `svt_rooms` WHERE `id_wp_post` > 1 AND `is_template` = 0");

if($result) {
    if($result->num_rows > 0) {
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $room_id = $row['id'];
            $mysqli->query("CREATE TEMPORARY TABLE svt_markers_tmp SELECT * FROM svt_markers WHERE id = '$marker_id';");
            $mysqli->query("UPDATE svt_markers_tmp SET id=(SELECT MAX(id)+1 as id FROM svt_markers), id_room=$room_id;");
            $mysqli->query("INSERT INTO svt_markers SELECT * FROM svt_markers_tmp;");
            $mysqli->query("DROP TEMPORARY TABLE IF EXISTS svt_markers_tmp;");
        }
        ob_end_clean();
        echo json_encode(array("status"=>"ok", "markers"=>$result->num_rows));
    }
} else {
    ob_end_clean();
    echo json_encode(array("status"=>"error"));
}