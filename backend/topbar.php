<?php
switch($user_info['plan_status']) {
    case 'active':
        $icon_plan = "<i class='fa fa-circle mt-1' style='color: green'></i>";
        break;
    case 'expiring':
        $icon_plan = "<i class='fa fa-circle mt-1' style='color: darkorange'></i>";
        break;
    case 'expired':
    case 'invalid_payment':
        $icon_plan = "<i class='fa fa-circle mt-1' style='color: red'></i>";
        break;
}
$settings = get_settings();
if(empty($_SESSION['lang'])) {
    $lang = $settings['language'];
} else {
    $lang = $_SESSION['lang'];
}
?>

<nav class="navbar navbar-expand navbar-light bg-white topbar mb-3 static-top shadow">
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>
    <ul class="navbar-nav ml-auto">
        <?php if($user_info['role']!='editor') : ?>
        <?php if($user_info['id_plan']!=0) : ?>
            <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="planDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="nav-link text-gray-600 small"><span class="px-2 py-1" style="margin-top:1px;border:1px solid #c4c4c4;border-radius:20px"><?php echo $icon_plan; ?>&nbsp;&nbsp;<?php echo $user_info['plan']; ?></span></span>
            </a>
            <div style="cursor: default" class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="planDropdown">
                <div class="dropdown-item">
                    <?php echo _("Status"); ?>:&nbsp;<?php
                    switch($user_info['plan_status']) {
                        case 'active':
                            echo "<span style='color:green'><b>"._("Active")."</b></span>";
                            break;
                        case 'expiring':
                            $expires_msg = "";
                            if($user_info['expire_plan_date']) {
                                $diff_days = dateDiffInDays(date('Y-m-d',strtotime($user_info['expire_plan_date'])),date('Y-m-d',strtotime('today')));
                                $expires_msg = sprintf(_("- expires in %s days"),abs($diff_days));
                            }
                            echo "<span style='color:darkorange'><b>"._("Active")."</b></span> $expires_msg";
                            break;
                        case 'expired':
                            echo "<span style='color:red'><b>"._("Expired")."</b></span>";
                            break;
                        case 'invalid_payment':
                            echo "<span style='color:red'><b>"._("Invalid payment")."</b></span>";
                            break;
                    }
                    ?>
                </div>
                <div class="dropdown-item">
                    <?php echo _("Virtual Tours"); ?>:&nbsp;<?php echo $user_stats['count_virtual_tours']." "._("of")."&nbsp;".(($plan_info['n_virtual_tours']<0) ? '<i style="vertical-align: middle;color: #737373" class="fas fa-infinity"></i>' : '<b>'.$plan_info['n_virtual_tours']).'</b>'; ?><br>
                    <?php echo _("Rooms"); ?>:&nbsp;<?php echo $user_stats['count_rooms']." "._("of")."&nbsp;".(($plan_info['n_rooms']<0) ? '<i style="vertical-align: middle;color: #737373" class="fas fa-infinity"></i>' : '<b>'.$plan_info['n_rooms']).'</b>'; ?><br>
                    <?php echo _("Markers"); ?>:&nbsp;<?php echo $user_stats['count_markers']." "._("of")."&nbsp;".(($plan_info['n_markers']<0) ? '<i style="vertical-align: middle;color: #737373" class="fas fa-infinity"></i>' : '<b>'.$plan_info['n_markers']).'</b>'; ?><br>
                    <?php echo _("POIs"); ?>:&nbsp;<?php echo $user_stats['count_pois']." "._("of")."&nbsp;".(($plan_info['n_pois']<0) ? '<i style="vertical-align: middle;color: #737373" class="fas fa-infinity"></i>' : '<b>'.$plan_info['n_pois']).'</b>'; ?><br>
                </div>
                <?php if($settings['change_plan']) { ?>
                <div class="dropdown-item">
                    <a href="index.php?p=change_plan" class="btn btn-primary btn-block btn-sm">
                        <span class="align-middle"><?php echo strtoupper(_("change plan")); ?></span>&nbsp;&nbsp;&nbsp;<i class="fas fa-random align-middle"></i>
                    </a>
                </div>
                <?php } ?>
            </div>
        </li>
        <?php endif; ?>
        <?php endif; ?>
        <li class="nav-item dropdown no-arrow lang_switcher">
            <a class="nav-link dropdown-toggle" href="#" id="langDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" <?php echo ($settings['languages_count']==1) ? 'style="cursor:default;pointer-events:none;"' : ''; ?> >
                <img style="height: 14px;" src="img/flags_lang/<?php echo $lang; ?>.png" />
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="langDropdown">
                <?php if(check_language_enabled('ar_SA',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('ar_SA');" class="<?php echo ($lang=='ar_SA') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/ar_SA.png" /> <span class="ml-2"><?php echo _("Arabic"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('zh_CN',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('zh_CN');" class="<?php echo ($lang=='zh_CN') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/zh_CN.png" /> <span class="ml-2"><?php echo _("Chinese Simplified"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('zh_HK',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('zh_HK');" class="<?php echo ($lang=='zh_HK') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/zh_HK.png" /> <span class="ml-2"><?php echo _("Chinese Traditional (Hong Kong)"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('zh_TW',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('zh_TW');" class="<?php echo ($lang=='zh_TW') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/zh_TW.png" /> <span class="ml-2"><?php echo _("Chinese Traditional (Taiwan)"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('cs_CZ',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('cs_CZ');" class="<?php echo ($lang=='cs_CZ') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/cs_CZ.png" /> <span class="ml-2"><?php echo _("Czech"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('nl_NL',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('nl_NL');" class="<?php echo ($lang=='nl_NL') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/nl_NL.png" /> <span class="ml-2"><?php echo _("Dutch"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('en_US',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('en_US');" class="<?php echo ($lang=='en_US') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/en_US.png" /> <span class="ml-2"><?php echo _("English"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('fil_PH',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('fil_PH');" class="<?php echo ($lang=='fil_PH') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/fil_PH.png" /> <span class="ml-2"><?php echo _("Filipino"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('fr_FR',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('fr_FR');" class="<?php echo ($lang=='zh_CN') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/fr_FR.png" /> <span class="ml-2"><?php echo _("French"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('de_DE',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('de_DE');" class="<?php echo ($lang=='de_DE') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/de_DE.png" /> <span class="ml-2"><?php echo _("German"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('hi_IN',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('hi_IN');" class="<?php echo ($lang=='hi_IN') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/hi_IN.png" /> <span class="ml-2"><?php echo _("Hindi"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('hu_HU',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('hu_HU');" class="<?php echo ($lang=='hu_HU') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/hu_HU.png" /> <span class="ml-2"><?php echo _("Hungarian"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('rw_RW',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('rw_RW');" class="<?php echo ($lang=='rw_RW') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/rw_RW.png" /> <span class="ml-2"><?php echo _("Kinyarwanda"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('it_IT',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('it_IT');" class="<?php echo ($lang=='it_IT') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/it_IT.png" /> <span class="ml-2"><?php echo _("Italian"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('ja_JP',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('ja_JP');" class="<?php echo ($lang=='ja_JP') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/ja_JP.png" /> <span class="ml-2"><?php echo _("Japanese"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('fa_IR',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('fa_IR');" class="<?php echo ($lang=='fa_IR') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/fa_IR.png" /> <span class="ml-2"><?php echo _("Persian"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('pl_PL',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('pl_PL');" class="<?php echo ($lang=='pl_PL') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/pl_PL.png" /> <span class="ml-2"><?php echo _("Polish"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('pt_BR',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('pt_BR');" class="<?php echo ($lang=='pt_BR') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/pt_BR.png" /> <span class="ml-2"><?php echo _("Portuguese Brazilian"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('pt_PT',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('pt_PT');" class="<?php echo ($lang=='pt_PT') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/pt_PT.png" /> <span class="ml-2"><?php echo _("Portuguese European"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('es_ES',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('es_ES');" class="<?php echo ($lang=='es_ES') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/es_ES.png" /> <span class="ml-2"><?php echo _("Spanish"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('ro_RO',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('ro_RO');" class="<?php echo ($lang=='ro_RO') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/ro_RO.png" /> <span class="ml-2"><?php echo _("Romanian"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('ru_RU',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('ru_RU');" class="<?php echo ($lang=='ru_RU') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/ru_RU.png" /> <span class="ml-2"><?php echo _("Russian"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('sv_SE',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('sv_SE');" class="<?php echo ($lang=='sv_SE') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/sv_SE.png" /> <span class="ml-2"><?php echo _("Swedish"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('tg_TJ',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('tg_TJ');" class="<?php echo ($lang=='tg_TJ') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/tg_TJ.png" /> <span class="ml-2"><?php echo _("Tajik"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('tr_TR',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('tr_TR');" class="<?php echo ($lang=='tr_TR') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/tr_TR.png" /> <span class="ml-2"><?php echo _("Turkish"); ?></span></span> <?php endif; ?>
                <?php if(check_language_enabled('vi_VN',$settings['languages_enabled'])) : ?> <span style="cursor: pointer;" onclick="switch_language('vi_VN');" class="<?php echo ($lang=='vi_VN') ? 'lang_active' : ''; ?> noselect dropdown-item align-middle"><img class="mb-1" src="img/flags_lang/vi_VN.png" /> <span class="ml-2"><?php echo _("Vietnamese"); ?></span></span> <?php endif; ?>
            </div>
        </li>
        <div class="topbar-divider d-none d-sm-block"></div>
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $user_info['username']; ?></span>&nbsp;
                <img class="img-profile rounded-circle" src="<?php echo $user_info['avatar']; ?>">
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <?php if(!empty($settings['help_url'])) : ?>
                    <a class="dropdown-item" target="_blank" href="<?php echo $settings['help_url']; ?>">
                        <i class="fas fa-question fa-sm fa-fw mr-2 text-gray-400"></i>
                        <?php echo _("Help"); ?>
                    </a>
                <?php endif; ?>
                <a class="dropdown-item" href="index.php?p=edit_profile">
                    <i class="fas fa-lock fa-sm fa-fw mr-2 text-gray-400"></i>
                    <?php echo _("Edit profile"); ?>
                </a>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    <?php echo _("Logout"); ?>
                </a>
            </div>
        </li>
    </ul>
</nav>
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo _("Ready to Leave?"); ?></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"><?php echo _("Select Logout below if you are ready to end your current session."); ?></div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal"><?php echo _("Cancel"); ?></button>
                <button class="btn btn-primary" onclick="logout();"><?php echo _("Logout"); ?></button>
            </div>
        </div>
    </div>
</div>