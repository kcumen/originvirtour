<?php
session_start();
$id_user = $_SESSION['id_user'];
$can_create = check_plan('virtual_tour',$id_user);
$settings = get_settings();
$change_plan = $settings['change_plan'];
if($change_plan) {
    $msg_change_plan = "<a class='text-white' href='index.php?p=change_plan'><b>"._("Click here to change your plan")."</b></a>";
} else {
    $msg_change_plan = "";
}
$enable_external_vt = $settings['enable_external_vt'];
$enable_sample = $settings['enable_sample'];
$hide_external = "";
$hide_sample = "";
$col_name = 3;
$col_author = 2;
$col_external = 2;
$col_sample = 1;
$col_button = 4;
$col_name_md = 3;
$col_author_md = 3;
$col_external_md = 3;
$col_sample_md = 3;
if($enable_external_vt==0 && $enable_sample==0) {
    $hide_external = "d-none";
    $hide_sample = "d-none";
    $col_name = 4;
    $col_author = 4;
    $col_button = 4;
    $col_name_md = 6;
    $col_author_md = 6;
} else if($enable_sample==0 && $enable_external_vt==1) {
    $hide_sample = "d-none";
    $col_name = 3;
    $col_author = 3;
    $col_external = 2;
    $col_button = 4;
    $col_name_md = 4;
    $col_author_md = 4;
    $col_external_md = 4;
} else if($enable_external_vt==0 && $enable_sample==1) {
    $hide_external = "d-none";
    $col_name = 3;
    $col_author = 3;
    $col_sample = 2;
    $col_button = 4;
    $col_name_md = 4;
    $col_author_md = 4;
    $col_sample_md = 4;
}
if(isset($_GET['cat'])) {
    $id_cat_sel = $_GET['cat'];
} else {
    $id_cat_sel = 0;
}
$plan_permissions = get_plan_permission($_SESSION['id_user']);
?>

<div class="d-sm-flex align-items-center justify-content-between mb-3">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-route text-gray-700"></i> <?php echo _("VIRTUAL TOURS"); ?></h1>
</div>
<div class="row">
    <div class="col-md-12">
        <?php if($user_info['role']!='editor') { ?>
            <?php if(($user_info['plan_status']=='active') || ($user_info['plan_status']=='expiring')) { ?>
                <?php if($can_create) { ?>
                    <div id="add_vt_form" class="card mb-4 py-3 border-left-success">
                        <div class="card-body" style="padding-top: 0;padding-bottom: 0;">
                            <form autocomplete="off">
                                <div id="modal_new_virtualtour" class="row align-items-end">
                                    <div class="col-md-<?php echo $col_name_md; ?> col-lg-<?php echo $col_name; ?>">
                                        <div id="name_div" class="form-group mb-0">
                                            <label class="mb-0" for="name"><?php echo _("Name"); ?></label>
                                            <input type="text" class="form-control" id="name" />
                                        </div>
                                    </div>
                                    <div class="col-md-<?php echo $col_author_md; ?> col-lg-<?php echo $col_author; ?>">
                                        <div class="form-group mb-0">
                                            <label class="mb-0" for="name"><?php echo _("Author"); ?></label>
                                            <input type="text" class="form-control" id="author" value="<?php echo $user_info['username']; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-<?php echo $col_external_md; ?> col-lg-<?php echo $col_external; ?> <?php echo $hide_external; ?>">
                                        <div class="form-group mb-0">
                                            <label class="mb-0" for="external"><?php echo _("Type"); ?></label>
                                            <select onchange="change_vt_type();" id="external" class="form-control">
                                                <option selected id="0"><?php echo _("Default"); ?></option>
                                                <option id="1"><?php echo _("External"); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-<?php echo $col_sample_md; ?> col-lg-<?php echo $col_sample; ?> <?php echo $hide_sample; ?>">
                                        <div style="margin-bottom: 12px;" class="form-group">
                                            <label class="mb-0" for="sample_data"><?php echo _("Sample"); ?> <i title="<?php echo _("includes sample data in this tour"); ?>" class="help_t fas fa-question-circle"></i></label><br>
                                            <input type="checkbox" id="sample_data" />
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-<?php echo $col_button; ?> mt-3 mt-lg-0 text-lg-right text-center">
                                        <div class="form-group mb-0">
                                            <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_create_tour" onclick="add_virtualtour(false);" type="button"  class="btn btn-success"><i class="fas fa-plus"></i> <?php echo _("Create"); ?></button>
                                            <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_create_edit_tour" onclick="add_virtualtour(true);" type="button"  class="btn btn-warning"><i class="fas fa-plus"></i> <?php echo _("Edit"); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="card bg-warning text-white shadow mb-4">
                        <div class="card-body">
                            <?php echo _("You have reached the maximum number of Virtual Tours allowed from your plan!")." ".$msg_change_plan; ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="card bg-warning text-white shadow mb-4">
                    <div class="card-body">
                        <?php echo sprintf(_('Your "%s" plan has expired!'),$user_info['plan'])." ".$msg_change_plan; ?>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
        <div id="virtual_tours_list">
            <div class="card mb-4 py-3 border-left-primary">
                <div class="card-body" style="padding-top: 0;padding-bottom: 0;">
                    <div class="row">
                        <div class="col-md-8 text-center text-sm-center text-md-left text-lg-left">
                            <?php echo _("LOADING VIRTUAL TOURS ..."); ?>
                        </div>
                        <div class="col-md-4 text-center text-sm-center text-md-right text-lg-right">
                            <a href="#" class="btn btn-primary btn-circle">
                                <i class="fas fa-spin fa-spinner"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal_delete_virtualtour" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo _("Delete Virtual Tour"); ?></h5>
            </div>
            <div class="modal-body">
                <p><?php echo _("Are you sure you want to delete the entire virtual tour, included rooms, markers, pois and map?"); ?></p>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_delete_virtualtour" onclick="" type="button" class="btn btn-danger"><i class="fas fa-trash"></i> <?php echo _("Yes, Delete"); ?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> <?php echo _("Close"); ?></button>
            </div>
        </div>
    </div>
</div>

<div id="modal_duplicate_virtualtour" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo _("Duplicate Virtual Tour"); ?></h5>
            </div>
            <div class="modal-body">
                <p><?php echo _("Are you sure you want to duplicate the entire virtual tour?"); ?></p>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="duplicate_info_box"><?php echo _("Info Box"); ?></label><br>
                            <input onchange="change_duplicate_items_vt();" type="checkbox" id="duplicate_info_box" checked />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="duplicate_maps"><?php echo _("Maps"); ?></label><br>
                            <input onchange="change_duplicate_items_vt();" type="checkbox" id="duplicate_maps" checked />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="duplicate_products"><?php echo _("Products"); ?></label><br>
                            <input onchange="change_duplicate_items_vt();" type="checkbox" id="duplicate_products" checked />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="duplicate_gallery"><?php echo _("Gallery"); ?></label><br>
                            <input onchange="change_duplicate_items_vt();" type="checkbox" id="duplicate_gallery" checked />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="duplicate_rooms"><?php echo _("Rooms"); ?></label><br>
                            <input onchange="change_duplicate_items_vt();" type="checkbox" id="duplicate_rooms" checked />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="duplicate_presentation"><?php echo _("Presentation"); ?></label><br>
                            <input onchange="change_duplicate_items_vt();" type="checkbox" id="duplicate_presentation" checked />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="duplicate_markers"><?php echo _("Markers"); ?></label><br>
                            <input onchange="change_duplicate_items_vt();" type="checkbox" id="duplicate_markers" checked />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="duplicate_pois"><?php echo _("POIs"); ?></label><br>
                            <input onchange="change_duplicate_items_vt();" type="checkbox" id="duplicate_pois" checked />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_duplicate_virtualtour" onclick="" type="button" class="btn btn-success"><i class="fas fa-copy"></i> <?php echo _("Yes, Duplicate"); ?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> <?php echo _("Close"); ?></button>
            </div>
        </div>
    </div>
</div>

<div id="modal_export_virtualtour" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo _("Download Virtual Tour"); ?></h5>
            </div>
            <div class="modal-body">
                <p><?php echo _("Are you sure you want to download the virtual tour?"); ?></p>
                <span class="alert-danger <?php echo (($_SERVER['SERVER_ADDR']=='5.9.29.89') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer'])) ? '':'d-none'; ?>"><?php echo _("You cannot download virtual tour from this demo server"); ?></span>
            </div>
            <div class="modal-footer">
                <button <?php echo (($_SERVER['SERVER_ADDR']=='5.9.29.89') && ($_SERVER['REMOTE_ADDR']!=$_SESSION['ip_developer'])) ? 'disabled':''; ?> id="btn_export_virtualtour" onclick="" type="button" class="btn btn-success"><i class="fas fa-download"></i> <?php echo _("Yes, Download"); ?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> <?php echo _("Close"); ?></button>
            </div>
        </div>
    </div>
</div>

<div id="modal_sample_tour" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo _("New Virtual Tour"); ?></h5>
            </div>
            <div class="modal-body">
                <p><?php echo _("Are you sure you want to create a tour containing sample data?"); ?></p>
            </div>
            <div class="modal-footer">
                <button <?php echo ($demo) ? 'disabled':''; ?> id="btn_create_sample_tour" onclick="create_sample_tour();" type="button" class="btn btn-success"><i class="fas fa-plus"></i> <?php echo _("Yes, Create"); ?></button>
                <button type="button" class="btn btn-secondary" onclick="close_virtualtour_sample();"><i class="fas fa-times"></i> <?php echo _("Close"); ?></button>
            </div>
        </div>
    </div>
</div>

<script>
    (function($) {
        "use strict"; // Start of use strict
        window.id_user = '<?php echo $id_user; ?>';
        window.user_role = '<?php echo $user_info['role']; ?>';
        window.can_create = <?php echo $can_create; ?>;
        window.id_cat_sel = <?php echo $id_cat_sel; ?>;
        window.enable_export_vt = <?php echo $plan_permissions['enable_export_vt']; ?>;
        window.create_and_edit = false;
        $(document).ready(function () {
            $('.help_t').tooltip();
            get_virtual_tours(window.id_cat_sel);
        });
    })(jQuery); // End of use strict
</script>